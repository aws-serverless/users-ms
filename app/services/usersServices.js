const usersServices = module.exports;

const usersRepository = require('../repository/usersRepository');
const httpClient = require('../utils/HttpClient');

usersServices.findAll = async () => {
  const { ip } = await httpClient.get('https://api.ipify.org?format=json');

  return { message: `hola onichan uwu <3! la ip del servidor es: ${ip}` };
};

usersServices.create = async (body) => {
  const [user] = await usersRepository.create(body);

  return user;
};
